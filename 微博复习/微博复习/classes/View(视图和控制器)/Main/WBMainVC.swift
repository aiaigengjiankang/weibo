//
//  WBMainVC.swift
//  微博复习
//
//  Created by pop on 2017/1/13.
//  Copyright © 2017年 chengcikeji. All rights reserved.
//

import UIKit

class WBMainVC: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.cz_random()
        setupChildVcs()
    }


}

extension WBMainVC {
    
     func setupChildVcs() {
        let array = [
            ["clsName":"WBHomeVC","imgName":"","title":"首页"],["clsName":"WBMessageVC","imgName":"","title":"消息"],
        ["clsName":"WBDiscoverVC","imgName":"","title":"发现"],
        ["clsName":"WBHomeVC","imgName":"","title":"我"]
        ]
        
        var arrayM = [UIViewController]()
        
        for dict in array {
            arrayM.append(controllers(dict:dict))
        }
        
        viewControllers = arrayM
        
    }


    private func controllers(dict:[String:String]) -> UIViewController {
    
        guard let clsName = dict["clsName"],
                   let imgName = dict["imgName"],
                   let title = dict["title"],
                   let cls = NSClassFromString(Bundle.main.nameSpace + "." + clsName) as? UIViewController.Type
            else {
                return UIViewController()
        }
     let vc = cls.init()
     vc.title = title
     vc.tabBarItem.image = UIImage.init(named:imgName)
     let nav = WBNaviVC.init(rootViewController:vc)
     return nav
    }

}


//
//  Bundle+Extension.swift
//  微博复习
//
//  Created by pop on 2017/1/15.
//  Copyright © 2017年 chengcikeji. All rights reserved.
//

import UIKit

extension Bundle {
    
    var nameSpace : String {
        
    return infoDictionary?["CFBundleName"] as? String ?? ""
    
    }
    
}
